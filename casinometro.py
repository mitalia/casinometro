#!/usr/bin/env python3
from datetime import *
from email.utils import format_datetime
import requests
from collections import namedtuple
import statistics
base_url = 'http://www.viaggiatreno.it/infomobilita/resteasy/viaggiatreno/'

TrainId = namedtuple('TrainId', ['start_station', 'code', 'departure_date'])
TrainId.__repr__ = lambda self: "%s(%s)" % (self.code, self.start_station)

station_names = {}

def station_name(station_code):
    return '%s(%s)' % (station_code, station_names.get(station_code))

def do_get(*args, **kwargs):
    #print(args, kwargs)
    return requests.get(*args, **kwargs)

def do_train_request(request, train_id):
    resp = do_get(base_url + "%s/%s/%s/%s" %
           (request, train_id.start_station, train_id.code, train_id.departure_date))
    if not resp.ok:
        return None
    return resp.json()

def do_station_request(request, station_id, date):
    resp = do_get(base_url + "%s/%s/%s" %
           (request, station_id, format_datetime(date)))
    if not resp.ok:
        return None
    return resp.json()

def get_train_status(train_id):
    ret = do_train_request('andamentoTreno', train_id)
    for stop in ret['fermate']:
        station_names[stop['id']] = stop['stazione']
    return ret

def get_train_canvas(train_id):
    return do_train_request('tratteCanvas', train_id)

def get_station_arrivals(station_id, date):
    return do_station_request('arrivi', station_id, date) or []

def get_station_departures(station_id, date):
    return do_station_request('partenze', station_id, date) or []

def guess_train(code):
    ret = []
    for l in do_get(base_url + "cercaNumeroTrenoTrenoAutocomplete/%s" % (code,)).text.split('\n'):
        l = l.strip()
        if not l:
            continue
        desc,rid = l.split('|')
        code,station,departure_date=rid.split('-')
        ret.append((TrainId(station, code, departure_date), desc))
    return ret

if __name__ == '__main__':
    import sys
    train_id,desc = guess_train(sys.argv[1])[0]
    print('Found', desc, train_id)
    orig_status = get_train_status(train_id)
    print('Stops:')
    orig_stops = [stop['id'] for stop in orig_status['fermate']]
    track_changes = {}
    for stop in orig_stops:
        print("   ", station_name(stop))
        track_changes[stop] = [0, 0]
    running_trains = set()
    now = datetime.now(timezone.utc)
    for stop in orig_stops:
        for t in get_station_arrivals(stop, now) + get_station_departures(stop, now):
            running_trains.add(TrainId(t["codOrigine"], t["numeroTreno"], t["dataPartenzaTreno"]))
    print("Current trains passing in those stations:", running_trains)

    delays = []

    for t in running_trains:
        status = get_train_status(t)
        stops = status['fermate']
        good_tot = good_arrived = 0
        last_arr_idx = -1
        def arrived(st):
            return 1*(st['arrivoReale'] is not None) +  2*(st['partenzaReale'] is not None)

        for i,stop in enumerate(stops):
            if arrived(stop):
                last_arr_idx = i
            if stop['id'] in orig_stops:
                good_tot += 1
                if arrived(stop):
                    good_arrived += 1
                tc = track_changes[stop['id']]
                tc[1] += 1
                bpp = stop['binarioProgrammatoPartenzaDescrizione']
                bep = stop['binarioEffettivoPartenzaDescrizione']
                bpa = stop['binarioProgrammatoArrivoDescrizione']
                bea = stop['binarioEffettivoArrivoDescrizione']
                if ((bpp is not None and bep is not None and bpp != bep) or
                    (bpa is not None and bea is not None and bpa != bea)):
                    tc[0] += 1
        if good_tot != good_arrived and good_arrived != 0:
            delay = int(status['ritardo'])
            delays.append(delay)
            print(t, "in interesting zone")
            print("    delay:", delay)
            if last_arr_idx >= 0:
                ls = stops[last_arr_idx]
                lsa = arrived(ls)
                if lsa == 1:
                    print("    at", station_name(ls['id']))
                elif lsa & 2:
                    print("    departed from", station_name(ls['id']))
                    print("    traveling to", station_name(stops[last_arr_idx+1]['id']))
    print("=== SUMMARY ===")
    print(len(delays), "interesting trains")
    print("Average delay: %0.2f ± %0.2f" % (statistics.mean(delays), statistics.stdev(delays)))
    print("Track changes:")
    for stop in orig_stops:
        if track_changes[stop][0] != 0:
            print("    %s: %d/%d" % (station_name(stop), *track_changes[stop]))
